# Story 6
This Gitlab repository is the result of the work from **Muhammad Raihan**

## Pipeline and Coverage
[![pipeline status](https://gitlab.com/raihan2604/Story-6/badges/master/pipeline.svg)](https://gitlab.com/raihan2604/Story-6/commits/master)
[![coverage report](https://gitlab.com/raihan2604/Story-6/badges/master/coverage.svg)](https://gitlab.com/raihan2604/Story-6/commits/master)

## URL
This lab projects can be accessed from [https://status-mraihan.herokuapp.com](https://status-mraihan.herokuapp.com)

## Author
**Muhammad Raihan** - [raihan2604](https://gitlab.com/raihan2604)
