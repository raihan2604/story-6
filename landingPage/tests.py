from django.test import TestCase, Client, LiveServerTestCase
from django.http import HttpRequest
from .views import landing
from .forms import Status_form
from django.urls import reverse, resolve
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options


# Create your tests here.
class landingTest(TestCase):
    def test_url_is_exist(self):
        response = Client().get('')
        self.assertEqual(response.status_code, 200)

    def test_url_is_not_exist(self):
        response = Client().get('')
        self.assertFalse(response.status_code == 404)

    def test_using_landing_funct(self):
        found = resolve('/')
        self.assertEqual(found.func, landing)

    def test_ada_tulisan_status(self):
        request = HttpRequest()
        response = landing(request)
        html_response = response.content.decode('utf8')
        self.assertIn('status', html_response)

    def test_ada_tulisan_apa_kabar(self):
        request = HttpRequest()
        response = landing(request)
        html_response = response.content.decode('utf8')
        self.assertIn('Hello, How Are You?', html_response)

    def test_using_landingPage_html(self):
        response = Client().get('')
        self.assertTemplateUsed(response, 'landingPage.html')

    def test_status_form(self):
        form_data = {
        "message" : "hi there"
        }
        form = Status_form(data = form_data)
        self.assertTrue(form.is_valid())

    def test_form_redirect(self):
        form_data = {
        "message" : "hi there"
        }
        request = self.client.post('/', data = form_data)
        self.assertEqual(request.status_code, 302)

class functional_test(LiveServerTestCase):
    print('masup')
    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')        
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        self.browser  = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
        super(functional_test, self).setUp()


    def tearDown(self):
        self.browser.quit()
        super(functional_test, self).tearDown()

    def test_input_todo(self):
        self.browser.get('http://127.0.0.1:8000')
        status = self.browser.find_element_by_id('id_message')
        submit = self.browser.find_element_by_id('submit')
        status.send_keys("Coba Coba")
        submit.send_keys(Keys.RETURN)