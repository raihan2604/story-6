from django.db import models
from datetime import datetime

# Create your models here.
class Status_model(models.Model):
    message = models.CharField(max_length = 300)
    date = models.DateTimeField(auto_now_add = True)
