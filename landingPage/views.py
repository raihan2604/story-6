from django.shortcuts import render, redirect
from django.http import HttpResponse
from .forms import Status_form
from .models import Status_model

# Create your views here.

def landing(request):
    if request.method == 'POST':
        form = Status_form(request.POST)
        if form.is_valid():
            print("valid")
            status = form.save(commit=False)
            status.save()
            return redirect('/')
    else:
        form = Status_form()
    lst = Status_model.objects.all().order_by('-date')
    args = {
        'form' : form,
        'lst' : lst
    }
    return render(request, 'landingPage.html', args)