from django import forms
from .models import Status_model

class Status_form(forms.ModelForm):
    class Meta:
        model = Status_model
        fields = {'message'}